'use strict';
angular.module('io.cumul.util', []);
angular.module('io.cumul.heattable-example', []);
angular.module('io.cumul-heattable', []);
angular.module('io.cumul.templates', []);

var D3app = angular.module('D3app', [
    'ngRoute',
    'ngCookies',
    'ngSanitize',
    'ngAnimate',
    'io.cumul.util',
    'io.cumul.heattable-example',
    //'io.cumul.heattable',
    'io.cumul.templates',
    'ui.bootstrap',
    'duScroll',
]);

D3app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
    $routeProvider.when('/', {
        templateUrl: 'app/heattable-example/heattable-example.html',
        controller: 'HeattableExampleCtrl'
    }).when('/heattable', {
        //templateUrl: 'app/heattable/heattable.html',
        //controller: 'HeattableCtrl'
    }).otherwise({
        redirectTo: '/'
    });
}]);