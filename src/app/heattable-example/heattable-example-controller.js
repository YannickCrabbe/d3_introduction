'use strict';
var heatmapControllers = angular.module('io.cumul.heattable-example');
heatmapControllers.controller('HeattableExampleCtrl',
    function ($scope) {
        initScope();
        parseData('../data/nba.csv', 'Name', $scope.selectedVizMode);

        /**
         Formats and normalizes data, and calls the function to draw the heattable.
         finishParsing HAS to be in callback of d3.csv, as d3.csv can't return data.
         **/
        function parseData(dataPath, categoryName, normalizingMethod) {
            d3.csv(dataPath, function (data) {
                var t0 = performance.now();

                var parsedData = formatData(data, categoryName);
                var categoryData = parsedData[0];
                var measureData = parsedData[1];

                var t1 = performance.now();
                console.log("Data formatting took " + (t1 - t0).toFixed(0) + " milliseconds.");

                if (normalizingMethod == "default")
                    normalizeData(categoryData, measureData);
                else if (normalizingMethod == "rank")
                    rankAndNormalizeData(categoryData, measureData);
                else if (normalizingMethod == "quantile (3)")
                    quantilizeAndNormalizeData(categoryData, measureData, 3);
                else if (normalizingMethod == "quantile (4)")
                    quantilizeAndNormalizeData(categoryData, measureData, 4);
                else if (normalizingMethod == "quantile (5)")
                    quantilizeAndNormalizeData(categoryData, measureData, 5);
                else if (normalizingMethod == "quantile (6)")
                    quantilizeAndNormalizeData(categoryData, measureData, 6);
                else if (normalizingMethod == "quantile (7)")
                    quantilizeAndNormalizeData(categoryData, measureData, 7);

                var t2 = performance.now();
                console.log("Data normalization took " + (t2 - t1).toFixed(0) + " milliseconds.");

                finishParsing(categoryData, measureData);
            });
        }

        function finishParsing(categoryData, measureData) {
            drawHeatMap(categoryData,
                {
                    numberOfRows: categoryData.length,
                    numberOfMeasures: measureData.length,
                    colorscale: $scope.selectedColor.value,
                    showValues: $scope.selectedShowValueOption
                });
        }

        /**
         config array has:
         - numberOfRows: number of rows (=category)
         - numberOfMeasures: number of measures to show (columns)
         - colorScale to use
         - boolean to show values in rectangles or not
         **/
        function drawHeatMap(data, config) {
            var margin = {top: 140, right: 100, bottom: 200, left: 150};

            var width = 850 - margin.left - margin.right,
                height = 1000 - margin.top - margin.bottom;

            //svg object
            var graph = d3.select(".heatMapSection").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            //grid container border
            graph.append("rect")
                .attr("x", 0)
                .attr("y", 0)
                .attr("height", height)
                .attr("width", width)
                .style("stroke", "black")
                .style("fill", "none")
                .style("stroke-width", 1);

            //grid for axes + rectangles
            var grid = graph.append("g")
                .attr("class", "grid");

            //hover tooltip, opacity 0 initially
            var div = d3.select("body").append("div")
                .attr("class", "tooltip")
                .style("opacity", 0);

            var rectangleWidth = width / config.numberOfMeasures, //number of columns (measures)
                rectangleHeight = height / config.numberOfRows;  //number of rows

            //category axis
            var rows = grid.selectAll("g")
                .data(data)
                .enter().append("g")
                .attr("class", "row")
                .attr("transform", (d, i) => "translate(" + [0, i * rectangleHeight] + ")");

            var rowLabels = rows.append("text")
                .attr("class", "category-labels")
                .attr("text-anchor", "end")
                .attr("x", -10)
                .attr("y", rectangleHeight)
                .attr("dy", -rectangleHeight / 3.5)
                .text(d => d.categoryName);

            //measure axis
            var measureLabels = grid.append("g")
                .attr("class", "measure-labels")
                .selectAll("text")
                .data(d3.range(config.numberOfMeasures))
                .enter().append("text")
                .attr("y", d => (rectangleWidth * d) + rectangleWidth / 2)
                .attr("dy", 3)
                .attr("dx", 10)
                .attr("transform", "rotate(-90)")
                .attr("text-anchor", "begin")
                .text(d => data[0].measures[d].measure); //TODO: more elegant solution?

            var rects = rows.selectAll("rect")
                .data(d => d.measures).enter()
                .append("rect")
                .attr("class", "data-rectangle")
                .attr("width", rectangleWidth - 1)
                .attr("height", rectangleHeight - 1)
                .attr("x", (d, i) => i * rectangleWidth)
                .style("fill", d => config.colorscale(d.normalizedValue)) //d3-scale-chromatic plugin as colorscale
                .on("mouseover", showLabel)
                .on("mousemove", moveLabel)
                .on("mouseout", hideLabel);

            function showLabel(d) {
                var coords = [d3.event.pageX, d3.event.pageY];
                var top = coords[1] + 20,
                    left = coords[0] - 25;

                d3.select(".tooltip")
                    .style("top", top + "px")
                    .style("left", left + "px")
                    .style("opacity", 1)

                d3.select(".tooltip")
                    .html(d.value)
            }

            function moveLabel() {
                var coords = [d3.event.pageX, d3.event.pageY];
                var top = coords[1] + 20,
                    left = coords[0] - 25;

                d3.select(".tooltip")
                    .style("top", top + "px")
                    .style("left", left + "px");
            }

            function hideLabel() {
                d3.select(".tooltip")
                    .style("opacity", 0);
            }

            drawLegend(width, height, config.colorscale);
        };

        function drawLegend(heattableWidth, heattableHeight, colorScale) {
            var legendContainer = d3.select(".heatMapSection")
                .select("svg")
                .select(".grid")
                .append("g")
                .attr("class", "legend")
                .attr("x", heattableWidth + 40)
                .attr("y", 0)
                .attr("width", 30)
                .attr("height", heattableHeight / 2)

            var legendBorder = legendContainer.append("rect")
                .attr("x", heattableWidth + 40)
                .attr("y", 0)
                .attr("width", 30)
                .attr("height", heattableHeight / 2)
                .style("stroke", "black")
                .style("fill", "none")
                .style("stroke-width", 0.5);

            var linearGradient = legendContainer.append("linearGradient")
                .attr("id", "linear-gradient")
                .attr('x1', '0%')
                .attr('x2', '0%')
                .attr('y1', '0%')
                .attr('y2', '100%');

            linearGradient.selectAll("stop")
                .data([
                    {offset: "0%", color: colorScale(0)},
                    {offset: "12.5%", color: colorScale(0.125)},
                    {offset: "25%", color: colorScale(0.25)},
                    {offset: "37.5%", color: colorScale(0.375)},
                    {offset: "50%", color: colorScale(0.5)},
                    {offset: "62.5%", color: colorScale(0.625)},
                    {offset: "75%", color: colorScale(0.75)},
                    {offset: "87.5%", color: colorScale(0.875)},
                    {offset: "100%", color: colorScale(1)}
                ])
                .enter().append("stop")
                .attr("offset", function(d) { return d.offset; })
                .attr("stop-color", function(d) { return d.color; });

            var legend = legendContainer.append("rect")
                .attr("x", heattableWidth + 40)
                .attr("y", 0)
                .attr("width", 30)
                .attr("height", heattableHeight / 2)
                .style("fill", "url(#linear-gradient)");

            legendContainer.append("text")
                .attr("class", "legend-text")
                .attr("x", heattableWidth + 40)
                .attr("y", 0)
                .attr("dx", 4.5)
                .attr("dy", 10)
                .text("Less");

            legendContainer.append("text")
                .attr("class", "legend-text")
                .attr("x", heattableWidth + 40)
                .attr("y", heattableHeight / 2)
                .attr("dx", 4.5)
                .attr("dy", -5)
                .style("fill", "white")
                .text("More");
        }

        /**
         Returns 2 arrays.
         - The first array is the category data, dimensions: rows x measures, structured as follows:
         [categoryName: <name>, measures: [{measure1: x}, {measure2: y}, {measure3: z}, ...]]
         - The second array is all values per measure, dimensions: measures x rows.
         This array is then used to normalize the data.
         **/
        function formatData(data, categoryName) {
            var categoryData = [];
            var allMeasures = [];

            data.forEach(row => {
                var measureArray = [];
                var currentMeasure = [];

                for (var measure in row) {
                    if (measure != categoryName) {
                        measureArray.push({
                            measure: measure,
                            value: row[measure]
                        });
                        currentMeasure.push(row[measure]);
                    }
                }
                categoryData.push({
                    categoryName: row[categoryName],
                    measures: measureArray
                });
                allMeasures.push(currentMeasure);
            });

            var transpose = m => m[0].map((x, i) => m.map(x => x[i])); //Invert 2D array, needed later for calculating min and max of every measure
            allMeasures = transpose(allMeasures);

            return [categoryData, allMeasures];
        };

        /**
         Normalize every measure: map all values on a 0 to 1 scale and add to main data array.
         Normalize formula: (x - min(x)) / (max(x) - min(x))
         **/
        function normalizeData(categoryData, measureData) {
            measureData.forEach((measure, measureIndex) => {
                var measureExtent = d3.extent(measure, (d) => parseFloat(d));

                categoryData.forEach(row =>
                    row.measures[measureIndex].normalizedValue = (parseFloat((row.measures[measureIndex].value - measureExtent[0])) / (measureExtent[1] - measureExtent[0])).toFixed(2));
            });
        }

        /**
         First, for every measure rank each value from 0 to n. (n being the number of rows)
         Then, normalize every measure: map all values on a 0 to 1 (aka in increments of 1/n) scale and add to main data array.
         Normalize formula: rank(x) / (max (rank (x)) - min (rank (x)))
         **/
        function rankAndNormalizeData(categoryData, measureData) {
            measureData.forEach((measure, measureIndex) => {
                var sortedMeasure = measure.slice().sort((a, b) => a - b);
                var rankedMeasure = measure.slice().map(v => sortedMeasure.indexOf(v) + 1);

                categoryData.forEach((row, rowIndex) =>
                    row.measures[measureIndex].normalizedValue = rankedMeasure[rowIndex] / (d3.max(rankedMeasure) - d3.min(rankedMeasure)));
            });
        }

        function quantilizeAndNormalizeData(categoryData, measureData, numberOfQuantiles) {
            /**
             * First, calculate quantile values for scale range (mapping [0, 1])
             * e.g. for 5 quantiles: [0, 0.2, 0.4, 0.6, 0.8, 1]
             */
            var quantiles = [];
            quantiles.push(0);
            for (var i = 1; i < numberOfQuantiles; i++) {
                quantiles.push(i / (numberOfQuantiles - 1));
            }

            measureData.forEach((measure, measureIndex) => {
                var quantileScale = d3.scaleQuantile().domain(measure).range(quantiles);
                categoryData.forEach(row => row.measures[measureIndex].normalizedValue = quantileScale(row.measures[measureIndex].value));
            });
        }

        function initScope() {
            //options
            $scope.vizModes = ['default', 'rank', 'quantile (3)', 'quantile (4)', 'quantile (5)', 'quantile (6)', 'quantile (7)'];
            $scope.colorOptions = [
                {
                    name: 'Yellow - orange - red',
                    value: d3.interpolateYlOrRd
                },
                {
                    name: 'Yellow - orange - brown',
                    value: d3.interpolateYlOrBr
                },
                {
                    name: 'Yellow - green',
                    value: d3.interpolateYlGn
                },
                {
                    name: 'Yellow - blue',
                    value: d3.interpolateYlGnBu
                },
                {
                    name: 'White - purple',
                    value: d3.interpolatePuRd
                },
                {
                    name: 'White - blue',
                    value: d3.interpolatePuBu
                },
                {
                    name: 'White - blue - green',
                    value: d3.interpolatePuBuGn
                },
                {
                    name: 'White - red',
                    value: d3.interpolateOrRd
                },
                {
                    name: 'Plasma',
                    value: d3.interpolatePlasma
                },
                {
                    name: 'Viridis',
                    value: d3.interpolateViridis
                },
                {
                    name: 'Magma',
                    value: d3.interpolateMagma
                },
                {
                    name: 'Warm',
                    value: d3.interpolateWarm
                },
                {
                    name: 'Cool',
                    value: d3.interpolateCool
                },
                {
                    name: 'Rainbow',
                    value: d3.interpolateRainbow
                }];
            $scope.showValueOptions = ['yes', 'no'];
            //defaults
            $scope.selectedVizMode = $scope.vizModes[0];
            $scope.selectedColor = $scope.colorOptions[0];
            $scope.selectedShowValueOption = $scope.showValueOptions[1];
            //change events
            $scope.selectedVizModeChanged = function () {
                d3.select(".heatMapSection").selectAll("svg").remove();
                parseData('../data/nba.csv', 'Name', $scope.selectedVizMode)
            };

            $scope.selectedColorChanged = function () {
                d3.select(".heatMapSection").selectAll("svg").remove();
                parseData('../data/nba.csv', 'Name', $scope.selectedVizMode)
            };

            $scope.selectedValueOptionChanged = function () {
                d3.select(".heatMapSection").selectAll("svg").remove();
                parseData('../data/nba.csv', 'Name', $scope.selectedVizMode)
            }
        }
    });