/*!
 * gulp
 * $ npm install 
 gulp
 gulp-minify-css
 gulp-ng-annotate
 gulp-angular-htmlify
 streamqueue
 gulp-autoprefixer
 gulp-jshint
 gulp-concat
 gulp-uglify
 gulp-notify
 gulp-header
 gulp-rename
 gulp-embedlr
 gulp-ssh
 gulp-gzip
 gulp-tar
 del
 gulp-angular-templatecache
 express
 tiny-lr
 compression
 connect-livereload
 gulp-livereload

 browserify
 gulp-connect
 gulp-util

 --save-dev
 * $ npm install gulp-cache gulp-imagemin imagemin-pngcrush --save-dev
 */


// Load plugins
var util = require('util'),
    gulp = require('gulp'),
    fs = require('fs'),
    streamqueue = require('streamqueue'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    header = require('gulp-header'),
    gzip = require('gulp-gzip'),
    tar = require('gulp-tar'),
    compression = require('compression'),
    ngAnnotate = require('gulp-ng-annotate'),
    htmlify = require('gulp-angular-htmlify'),
    del = require('del'),
    templateCache = require('gulp-angular-templatecache'),
    express = require('express'),
    tinylr = require('tiny-lr')()
connectLiveReload = require('connect-livereload'),
    livereload = require('gulp-livereload'),
    embedlr = require('gulp-embedlr'),
    runSequence = require('run-sequence'),

    gutil = require('gulp-util'),
    browserify = require('gulp-browserify'),
    //cache = require('gulp-cache'),
    //imagemin = require('gulp-imagemin'),
    //pngcrush = require('imagemin-pngcrush'),
    connect = require('gulp-connect');

var config = {
    banner: [
        '/*',
        '* 2017',
        '* All Rights Reserved. ',
        '* ',
        '* NOTICE:  All information contained herein is, and remains',
        '* the property of Cumul.io.',
        '* The intellectual and technical concepts contained',
        '* herein are proprietary to Cumul.io',
        '* and are protected by trade secret or copyright law.',
        '* Dissemination of this information or reproduction of this material',
        '* is strictly forbidden unless prior written permission is obtained',
        '* from Cumul.io.',
        '*/',
        ''
    ],
    port: 4000,
    paths: {
        app: {
            //order is of importance
            scripts: [
                'src/app/*.js',
                'src/app/components/**/*.js',
                'src/app/home/**/*.js'
            ],
            styles: ['src/app/**/*.css'],                 //all css files
            templates: ['src/app/**/*.html'],                //all html templates
            //images    : ['src/app/**/*.{png,jpg,jpeg,gif}'],  //all image files
            static: ['src/static/**/*.*']                 //any other content such as the favicon
        },
        vendor: {
            scripts: [                                   //all vendor scripts, order is important!
                'src/plugins/jquery/jquery.min.js',
                'src/plugins/jquery-ui/jquery.ui.min.js',
                'src/plugins/modernizr/modernizr.min.js',
                'src/plugins/angular/angular.min.js',
                'src/plugins/angular/angular-route.min.js',
                'src/plugins/angular/angular-cookies.min.js',
                'src/plugins/angular/angular-animate.min.js',
                'src/plugins/angular/angular-sanitize.min.js',
                'src/plugins/angular-ui-bootstrap/ui-bootstrap-custom-tpls-2.5.0.min.js',
                'src/plugins/d3/d3.min.js',
                'src/plugins/momentjs/moment.min.js',
                'src/plugins/lity/lity.min.js',
                'src/plugins/angular-scroll/angular-scroll.min.js',  // ISSUE problems with gulp, minified, added headers
                'src/plugins/autofill/autofill-event.js',  //have headers - TO DO minify
                'src/plugins/bootstrap/bootstrap.min.js'
            ],
            styles: [                                   //all vendor styles, order is important
                'src/plugins/bootstrap/bootstrap.min.css',
                'src/plugins/fontawesome/css/font-awesome.css',
                'src/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css',
                'src/plugins/viz-icons/viz-icons.css',
                'src/plugins/lity/lity.min.css'
            ],
            fonts: [                                    //all fonts
                'src/plugins/fontawesome/fonts/*.*',
                'src/plugins/viz-icons/fonts/*.*',
                'src/fonts/*.*'
            ]
        }
    }
};

/*
 * SCRIPTS
 */

//app
var compileAppScripts = function () {
    return appScripts = gulp.src(config.paths.app.scripts).pipe(ngAnnotate());
};
var compileTemplates = function () {
    var templates;
    return templates = gulp.src(config.paths.app.templates).pipe(htmlify()).pipe(templateCache({
        root: 'app/', //make sure that all templates point to app/... and not /app/...
        standalone: false,
        module: 'io.cumul.templates'
    }));
};
var concatenateAllScripts = function (lint) {
    var queue = streamqueue(
        {objectMode: true},
        compileAppScripts(),
        compileTemplates()
    )
    if (lint) {
        //queue
        //  .pipe(jshint('.jshintrc'))
        //  .pipe(jshint.reporter('default'))
    }
    return queue.pipe(concat('D3app.home.js'));
};
var buildScripts = function (buildPath, minify) {
    if (buildPath === 'src') return appScripts = gulp.src(config.paths.app.scripts).pipe(livereload(tinylr));

    var scripts;
    var lint = false;
    if (buildPath === 'build') lint = true;
    scripts = concatenateAllScripts(lint);
    if (minify) {
        scripts = scripts
            //.pipe(gulp.dest(buildPath + '/js/'))
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
        ;
    }
    return scripts
        .pipe(header(config.banner.join('\n')))
        .pipe(gulp.dest(buildPath + '/js/'))
        .pipe(livereload(tinylr));
};

gulp.task('src_scripts', function () {
    return buildScripts('src');
});

gulp.task('build_scripts', function () {
    return buildScripts('build');
});

gulp.task('dist_scripts', function () {
    return buildScripts('dist', true);
});

//vendor
var compileVendorScripts = function () {
    return vendorScripts = gulp.src(config.paths.vendor.scripts).pipe(ngAnnotate());
};
var concatenateAllVendorScripts = function () {
    return streamqueue(
        {objectMode: true},
        compileVendorScripts()
    )
        .pipe(concat('vendor.home.min.js')); //we won't minify again, we presume all js are already minified - TO DO minify those not yet minified
};
var buildVendorScripts = function (buildPath) {
    if (buildPath === 'src') return vendorScripts = gulp.src(config.paths.vendor.scripts).pipe(livereload(tinylr));
    var scripts;
    scripts = concatenateAllVendorScripts();
    return scripts.pipe(gulp.dest(buildPath + '/js/')).pipe(livereload(tinylr));
};

gulp.task('src_vendor_scripts', function () {
    return buildVendorScripts('src');
});

gulp.task('build_vendor_scripts', function () {
    return buildVendorScripts('build');
});

gulp.task('dist_vendor_scripts', function () {
    return buildVendorScripts('dist', true);
});

/*
 * STYLES
 */

var compileVendorStyles = function () {
    return gulp.src(config.paths.vendor.styles).pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'));
};

var compileAppStyles = function () {
    return gulp.src(config.paths.app.styles).pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'));
};

var concatenateAllStyles = function () {
    return streamqueue(
        {objectMode: true},
        compileAppStyles()
    )
        .pipe(concat('D3app.home.css'));
};

var buildStyles = function (buildPath, minify) {
    if (buildPath === 'src') return gulp.src(config.paths.app.styles).pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4')).pipe(livereload(tinylr));
    var styles;
    styles = concatenateAllStyles();
    if (minify) {
        styles = styles
            //.pipe(gulp.dest(buildPath + '/css/'))
            .pipe(rename({suffix: '.min'}))
            .pipe(minifyCSS());
    }
    return styles
        .pipe(header(config.banner.join('\n')))
        .pipe(gulp.dest(buildPath + '/css/'))
        .pipe(livereload(tinylr));
};

gulp.task('src_styles', function () {
    return buildStyles('src');
});

gulp.task('build_styles', function () {
    return buildStyles('build');
});

gulp.task('dist_styles', function () {
    return buildStyles('dist', true);
});

//vendor
var compileVendorStyles = function () {
    return gulp.src(config.paths.vendor.styles).pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'));
};

var concatenateAllVendorStyles = function () {
    return streamqueue(
        {objectMode: true},
        compileVendorStyles()
    )
        .pipe(concat('vendor.home.css'));
};

var buildVendorStyles = function (buildPath, minify) {
    var styles;
    if (buildPath == null) {
        buildPath = 'build';
    }
    styles = concatenateAllVendorStyles();
    if (minify) {
        styles = styles
            //.pipe(gulp.dest(buildPath + '/css/'))
            .pipe(rename({suffix: '.min'}))
            .pipe(minifyCSS());
    }
    return styles.pipe(gulp.dest(buildPath + '/css/')).pipe(livereload(tinylr));
};
var watchVendorStyles = function () {
    return gulp.src(config.paths.vendor.styles).pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4')).pipe(livereload(tinylr));
}

gulp.task('src_vendor_styles', function () {
    return watchSourceVendorStyles();
});

gulp.task('build_vendor_styles', function () {
    return buildVendorStyles();
});

gulp.task('dist_vendor_styles', function () {
    return buildVendorStyles('dist', true);
});

/*
 * FONTS
 */

var buildFonts = function (buildPath) {
    if (buildPath === 'src') return gulp.src(config.paths.vendor.fonts).pipe(livereload(tinylr));
    return gulp.src(config.paths.vendor.fonts).pipe(gulp.dest(buildPath + '/fonts/'));
};

gulp.task('src_fonts', function () {
    return buildFonts('src');
});

gulp.task('build_fonts', function () {
    return buildFonts('build');
});

gulp.task('dist_fonts', function () {
    return buildFonts('dist');
});


/*
 * STATIC
 */

var buildStatic = function (buildPath) {
    if (buildPath === 'src') return gulp.src(config.paths.app.static).pipe(livereload(tinylr));
    return gulp.src(config.paths.app.static).pipe(gulp.dest(buildPath + '/static/'));
};

gulp.task('src_static', function () {
    return buildStatic('src');
});

gulp.task('build_static', function () {
    return buildStatic('build');
});

gulp.task('dist_static', function () {
    return buildStatic('dist');
});


/*
 * CLEANUP
 */

gulp.task('build_clean', function () {
    return del([
        'build/js',
        'build/css',
        'build/fonts',
        'build/static'
    ]);
});

gulp.task('dist_clean', function () {
    return del([
        'dist/js',
        'dist/css',
        'dist/fonts',
        'dist/static'
    ]);
});

/*
 * WATCH LIVE CHANGES
 */

gulp.task('src_watch', function () {
    gulp.watch([config.paths.app.scripts, config.paths.app.templates], ['src_scripts']);
    gulp.watch([config.paths.vendor.scripts], ['src_vendor_scripts']);
    gulp.watch([config.paths.app.styles], ['src_styles']);
    gulp.watch([config.paths.vendor.styles], ['src_vendor_styles']);
    gulp.watch([config.paths.vendor.fonts], ['src_fonts']);
    return gulp.watch([config.paths.app['static']], ['src_static']);
});

gulp.task('build_watch', function () {
    gulp.watch([config.paths.app.scripts, config.paths.app.templates], ['build_scripts']);
    gulp.watch([config.paths.vendor.scripts], ['build_vendor_scripts']);
    gulp.watch([config.paths.app.styles], ['build_styles']);
    gulp.watch([config.paths.vendor.styles], ['build_vendor_styles']);
    gulp.watch([config.paths.vendor.fonts], ['build_fonts']);
    return gulp.watch([config.paths.app['static']], ['build_static']);
});

gulp.task('dist_watch', function () {
    gulp.watch([config.paths.app.scripts, config.paths.app.templates], ['dist_scripts']);
    gulp.watch([config.paths.vendor.scripts], ['dist_vendor_scripts']);
    gulp.watch([config.paths.app.styles], ['dist_styles']);
    gulp.watch([config.paths.vendor.styles], ['dist_vendor_styles']);
    gulp.watch([config.paths.vendor.fonts], ['dist_fonts']);
    return gulp.watch([config.paths.app['static']], ['dist_static']);
});

/*
 * SERVERS
 */

var startServer = function (type, liverefresh) {
    var type = type ? type : 'src';
    var port = 4000;
    if (type === 'build') {
        port = 4001;
    }
    else if (type === 'dist') {
        port = 4002;
    }
    if (liverefresh) livereload.listen();
    var app = express();
    //app.disable('x-powered-by');
    app.use(compression());
    if (liverefresh) app.use(connectLiveReload({port: 35729}));
    app.use('/css', express.static(__dirname + '/' + type + '/css'));
    app.use('/fonts', express.static(__dirname + '/' + type + '/fonts'));
    app.use('/img', express.static(__dirname + '/' + type + '/img'));
    app.use('/js', express.static(__dirname + '/' + type + '/js'));
    app.use('/static', express.static(__dirname + '/' + type + '/static'));
    if (type === 'src') app.use('/plugins', express.static(__dirname + '/' + type + '/plugins'));
    app.use('/', express.static(__dirname + '/' + type + ''));
    app.all('/*', function (req, res, next) {
        res.sendFile('index.html', {root: __dirname + '/' + type + '/'});
    });
    return app.listen(port);
}

gulp.task('src_server', function () {
    return startServer('src');
});

gulp.task('src_server_reload', function () {
    return startServer('src', true);
});

gulp.task('build_server', function () {
    return startServer('build');
});

gulp.task('build_server_reload', function () {
    return startServer('build', true);
});

gulp.task('dist_server', function () {
    return startServer('dist');
});

gulp.task('dist_server_reload', function () {
    return startServer('dist', true);
});

/*
 * DEPLOY
 */

gulp.task('dist_sftp', function () {
    return gulp.src('dist/**/*')
        .pipe(tar('nightlybuild.home.tar'))
        .pipe(gzip())
        .pipe(ssh.sftp('write', 'io.cumul/builds/nightlybuild-io.cumul.home.tar.gz'));
})
gulp.task('webserver_script_sftp', function () {
    return gulp.src('webserver-home.js')
        .pipe(ssh.sftp('write', 'io.cumul/io.cumul.home/webserver-home.js'));
})
gulp.task('dist_deploy_script', ['dist_sftp'], function () {
    return ssh
        .shell(['bash -l /home/vi/io.cumul/scripts/deploy-io.cumul.home.sh'], {filePath: 'deploy.log'})
        .pipe(gulp.dest('logs'));
});


/*
 * MAJOR TASKS - USE ONLY THESE
 */

gulp.task('src_view', function (callback) {
    runSequence('src_server',
        callback);
});
gulp.task('src_view_reload', function (callback) {
    runSequence(['src_server_reload', 'src_watch'],
        callback);
});

gulp.task('build', function (callback) {
    runSequence('build_clean',
        ['build_scripts', 'build_vendor_scripts', 'build_styles', 'build_vendor_styles', 'build_fonts', 'build_static'],
        callback);
});
gulp.task('build_view', function (callback) {
    runSequence('build_clean',
        ['build_scripts', 'build_vendor_scripts', 'build_styles', 'build_vendor_styles', 'build_fonts', 'build_static'],
        'build_server',
        callback);
});
gulp.task('build_view_reload', function (callback) {
    runSequence('build_clean',
        ['build_scripts', 'build_vendor_scripts', 'build_styles', 'build_vendor_styles', 'build_fonts', 'build_static'],
        ['build_server_reload', 'build_watch'],
        callback);
});

gulp.task('dist', function (callback) {
    runSequence('dist_clean',
        ['dist_scripts', 'dist_vendor_scripts', 'dist_styles', 'dist_vendor_styles', 'dist_fonts', 'dist_static'],
        callback);
});
gulp.task('dist_view', function (callback) {
    runSequence('dist_clean',
        ['dist_scripts', 'dist_vendor_scripts', 'dist_styles', 'dist_vendor_styles', 'dist_fonts', 'dist_static'],
        'dist_server',
        callback);
});
gulp.task('dist_view_reload', function (callback) {
    runSequence('dist_clean',
        ['dist_scripts', 'dist_vendor_scripts', 'dist_styles', 'dist_vendor_styles', 'dist_fonts', 'dist_static'],
        ['dist_server_reload', 'dist_watch'],
        callback);
});
gulp.task('dist_deploy', function (callback) {
    runSequence('dist_clean',
        ['dist_scripts', 'dist_vendor_scripts', 'dist_styles', 'dist_vendor_styles', 'dist_fonts', 'dist_static'],
        'dist_deploy_script',
        callback);
});

gulp.task('default', function (callback) {
    runSequence('build_clean',
        ['build_scripts', 'build_vendor_scripts', 'build_styles', 'build_vendor_styles', 'build_fonts', 'build_static'],
        ['build_server_reload', 'build_watch'],
        callback);
});